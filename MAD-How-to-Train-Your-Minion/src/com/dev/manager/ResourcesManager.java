package com.dev.manager;

import java.io.IOException;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.background.ParallaxBackground;
import org.andengine.entity.scene.background.ParallaxBackground.ParallaxEntity;
import org.andengine.entity.scene.background.RepeatingSpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.AssetBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.bitmap.BitmapTextureFormat;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import android.graphics.Color;

import com.dev.minion.GameActivity;

/**
 * This class will be responsible for loading/unloading our game resources (art,
 * fonts, audio) it will also provide references to the most commonly needed
 * objects (camera, context, engine, VertexBufferObjectManager)
 */

public class ResourcesManager {
	// ---------------------------------------------
	// VARIABLES
	// ---------------------------------------------

	private static final ResourcesManager INSTANCE = new ResourcesManager();

	public Engine engine;
	public GameActivity activity;
	public BoundCamera camera;
	public VertexBufferObjectManager vbom;

	// Font used for displaying text like score, game over, etc...
	public Font font;
	// private int pWidth = 800;
	// private int pHeight = 450;
	// private String splashName = "minion-splash.jpg";

	// ---------------------------------------------
	// TEXTURES & TEXTURE REGIONS
	// ---------------------------------------------

	// Game
	private BuildableBitmapTextureAtlas gameTextureAtlas;
	// private BitmapTextureAtlas gameTextureAtlas;
	// public AssetBitmapTexture backgroundTexture;
	public RepeatingSpriteBackground rBackground;
	public ITextureRegion gameBackgroundRegion; // game scene background
	public ITextureRegion pauseRegion; // pause button
	public ITextureRegion platform1_region;
	public ITextureRegion platform2_region;
	public ITextureRegion platform3_region;
	public ITextureRegion coin_region;
	public ITiledTextureRegion playerRegion;

	AssetBitmapTexture mABitmapTexture = null;
	ITextureRegion mRepeatingTextureRegion;

	public ParallaxBackground parallaxBackground;

	// Splash
	private BitmapTextureAtlas splashTextureAtlas;
	public ITextureRegion splashRegion;

	// Menu
	public BuildableBitmapTextureAtlas menuTextureAtlas;
	public ITextureRegion menuBackgroundRegion;
	public ITextureRegion playRegion;
	// public ITextureRegion continueRegion;
	public ITextureRegion optionRegion;
	public ITextureRegion soundRegion;
	public ITextureRegion backRegion;
	public ITextureRegion bestScoreRegion;
	public ITextureRegion minionRegion;
	public Music menuMusic;

	private BuildableBitmapTextureAtlas parallaxTextureAtlas;

	private TextureRegion parallaxBackgroundRegion;
	
	// ---------------------------------------------
	// CLASS LOGIC
	// ---------------------------------------------

	/**
	 * This method loads all resources needed to build MainMenu
	 */
	public void loadMenuResources() {
		loadMenuGraphics();
		loadMenuAudio();
		loadMenuFonts();
	}

	/**
	 * This method loads all the resources needed to build GameScene
	 * 
	 * @throws IOException
	 */
	public void loadGameResources() throws IOException {
		loadGameGraphics();
		loadGameFonts();
		loadGameAudio();
	}

	/**
	 * This methods loads all the graphics of the MainMenu
	 */
	private void loadMenuGraphics() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
		menuTextureAtlas = new BuildableBitmapTextureAtlas(
				activity.getTextureManager(), 1024, 1024,
				TextureOptions.BILINEAR);
		menuBackgroundRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(menuTextureAtlas, activity, "background.png");
		playRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				menuTextureAtlas, activity, "play.png");
		// continueRegion =
		// BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas,
		// activity, "play.png");
		optionRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				menuTextureAtlas, activity, "options.png");
		soundRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				menuTextureAtlas, activity, "sound.png");
		backRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				menuTextureAtlas, activity, "back.png");
		bestScoreRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(menuTextureAtlas, activity, "best.png");
		minionRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				menuTextureAtlas, activity, "mnion.png");
		try {
			this.menuTextureAtlas
					.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
							0, 1, 0));
			this.menuTextureAtlas.load();
		} catch (final TextureAtlasBuilderException e) {
			Debug.e(e);
		}
	}

	public void loadMenuTextures() {
		menuTextureAtlas.load();
	}

	public void unloadMenuTextures() {
		menuTextureAtlas.unload();
	}

	private void loadMenuAudio() {
		MusicFactory.setAssetBasePath("sfx/");
		try {
			menuMusic = MusicFactory.createMusicFromAsset(
					activity.getMusicManager(), activity, "agnes.mp3");
			menuMusic.setLooping(true);
			menuMusic.play();
		} catch (final IOException e) {
			Debug.e(e);
		}

	}

	private void loadMenuFonts() {
		FontFactory.setAssetBasePath("font/");
		final ITexture mainFontTexture = new BitmapTextureAtlas(
				activity.getTextureManager(), 256, 256,
				TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		font = FontFactory.createStrokeFromAsset(activity.getFontManager(),
				mainFontTexture, activity.getAssets(), "font.ttf", 30, true,
				Color.WHITE, 2, Color.BLACK);
		font.load();
	}

	/**
	 * This method loads all the graphic for GameScene
	 * 
	 * @throws IOException
	 */
	private void loadGameGraphics() throws IOException {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/game/");

		gameTextureAtlas = new BuildableBitmapTextureAtlas(
				activity.getTextureManager(), 1024, 1024,
				TextureOptions.BILINEAR);
		// backgroundTexture = new
		// AssetBitmapTexture(activity.getTextureManager(),
		// activity.getAssets(), "gameBackground.png",
		// TextureOptions.REPEATING_NEAREST);
		// backgroundTexture.load();
		// gameBackgroundRegion =
		// TextureRegionFactory.extractFromTexture(backgroundTexture);
		// gameBackgroundRegion =
		// BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas,
		// activity, "gameBackground.png");
		// rBackground = new RepeatingSpriteBackground(800, 480,
		// gameBackgroundRegion, vbom);
		pauseRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				gameTextureAtlas, activity, "pause.png");
		platform1_region = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(gameTextureAtlas, activity, "platform1.png");
		platform2_region = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(gameTextureAtlas, activity, "platform2.png");
		platform3_region = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(gameTextureAtlas, activity, "platform3.png");
		coin_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				gameTextureAtlas, activity, "coin.png");
		playerRegion = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(gameTextureAtlas, activity,
						"minion_run.png", 4, 1);
		try {
			this.gameTextureAtlas
					.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
							0, 1, 0));
			this.gameTextureAtlas.load();
		} catch (final TextureAtlasBuilderException e) {
			Debug.e(e);
		}

		// mABitmapTexture = new AssetBitmapTexture(engine.getTextureManager(),
		// activity.getAssets(), "gfx/game/gameBackground.png",
		// BitmapTextureFormat.
		// RGB_565,TextureOptions.REPEATING_BILINEAR);
		// mABitmapTexture.load();
		//
		// mRepeatingTextureRegion =
		// TextureRegionFactory.extractFromTexture(mABitmapTexture);
		//
		// rBackground = new RepeatingSpriteBackground(camera.getWidth(),
		// camera.getHeight(), mRepeatingTextureRegion, 1f,
		// vbom);

//		parallaxTextureAtlas = new BuildableBitmapTextureAtlas(
//				engine.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
		parallaxBackgroundRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(gameTextureAtlas, activity.getAssets(),
						"gameBackground.png");
		parallaxTextureAtlas.load();

		Sprite topCloud = new Sprite(400, 240, parallaxBackgroundRegion, vbom);
//		parallaxBackground = new ParallaxBackground(152 / 255f, 212 / 255f,
//				220 / 255f) {
//
//			@Override
//			public void onUpdate(float pSecondsElapsed) {
//				final float cameraCurrentX = camera.getCenterX();
//				this.setParallaxValue(cameraCurrentX);				
//				super.onUpdate(pSecondsElapsed);
//			}
//		};

		parallaxBackground = new ParallaxBackground(0,0,0);
		parallaxBackground
				.attachParallaxEntity(new ParallaxEntity(0, topCloud));
	}

	private void loadGameFonts() {

	}

	private void loadGameAudio() {

	}

	public void unloadGameTextures() {
		//gameTextureAtlas.unload();
	}

	/**
	 * This will load our splash file
	 */
	public void loadSplashScreen() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		splashTextureAtlas = new BitmapTextureAtlas(
				activity.getTextureManager(), 1024, 1024,
				TextureOptions.BILINEAR);
		splashRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				splashTextureAtlas, activity, "minion-splash.jpg", 0, 0);
		splashTextureAtlas.load();

	}

	public void unloadSplashScreen() {
		splashTextureAtlas.unload();
		splashRegion = null;
	}

	/**
	 * @param engine
	 * @param activity
	 * @param camera
	 * @param vbom
	 * <br>
	 * <br>
	 *            We use this method at beginning of game loading, to prepare
	 *            Resources Manager properly, setting all needed parameters, so
	 *            we can latter access them from different classes (eg. scenes)
	 */
	public static void prepareManager(Engine engine, GameActivity activity,
			BoundCamera camera, VertexBufferObjectManager vbom) {
		getInstance().engine = engine;
		getInstance().activity = activity;
		getInstance().camera = camera;
		getInstance().vbom = vbom;
	}

	// ---------------------------------------------
	// GETTERS AND SETTERS
	// ---------------------------------------------

	public static ResourcesManager getInstance() {
		return INSTANCE;
	}
}
