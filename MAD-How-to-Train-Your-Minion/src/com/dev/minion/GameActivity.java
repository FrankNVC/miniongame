package com.dev.minion;

import java.io.IOException;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import com.dev.manager.ResourcesManager;
import com.dev.manager.SceneManager;

/**
 * @author Vuong Nguyen
 * @author nguvong.me@gmail.com
 * @version 1.0
 */

public class GameActivity extends BaseGameActivity {

	private BoundCamera camera;
	private ResourcesManager resourcesManager;
	
	// Camera setter
	
	private static final float SCROLL_TIME = 5;
	private static final float SCREEN_MIN_CENTER_X = 800 * 0.5f - 25;
	private static final float SCREEN_MAX_CENTER_X = 800 * 0.5f + 25;
	
	/**
	 * Create our camera This will set basic settings like orientation, full
	 * screen move, resolution policy, camera etc.
	 */
	@Override
	public EngineOptions onCreateEngineOptions() {
		camera = new BoundCamera(0, 0, 800, 400);
//		camera = new BoundCamera(0, 0, 800, 400){
//				@Override
//				public void onUpdate(float pSecondsElapsed) {
//					final float currentCenterX = this.getCenterX();
//
//					this.setCenter(currentCenterX + pSecondsElapsed * SCROLL_TIME, this.getCenterY());
//					super.onUpdate(pSecondsElapsed);
//				}
//
//			};
		EngineOptions engineOptions = new EngineOptions(true,
				ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(),
				this.camera);
		engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
		engineOptions.getRenderOptions().getConfigChooserOptions()
				.setRequestedMultiSampling(true);
		// Prevent auto turn off dislay
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		return engineOptions;
	}

	/**
	 * This attempts to achieve a set number of updates per second. Here we set
	 * 60 updates per second
	 */

	public Engine onCreateEngine(EngineOptions pEngineOptions) {
		return new LimitedFPSEngine(pEngineOptions, 60);
	}

	/**
	 * Initialize the resources manager and pass the required parameters inside
	 * onCreateResources
	 */
	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws IOException {
		ResourcesManager.prepareManager(mEngine, this, camera,
				getVertexBufferObjectManager());
		resourcesManager = ResourcesManager.getInstance();
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	/**
	 * First scene to display is splash scene
	 */
	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws IOException {
		SceneManager.getInstance().createSplashScene(pOnCreateSceneCallback); // Create
																				// Splash
																				// Scene
	}

	/**
	 * Populate MainMenu
	 */
	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback)
			throws IOException {
		mEngine.registerUpdateHandler(new TimerHandler(2f,
				new ITimerCallback() {
					public void onTimePassed(final TimerHandler pTimerHandler) {
						mEngine.unregisterUpdateHandler(pTimerHandler);
						SceneManager.getInstance().createMenuScene();
					}
				}));
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.exit(0);
	}
}
