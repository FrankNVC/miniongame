package com.dev.scene;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.util.GLState;
import org.andengine.util.adt.align.HorizontalAlign;

import com.dev.manager.ResourcesManager;
import com.dev.manager.SceneManager;
import com.dev.manager.SceneManager.SceneType;

/**
 * This class is the MainMenu of the game
 * @author Frank NgVietCuong
 *
 */
public class MainMenuScene extends BaseScene implements IOnMenuItemClickListener{

	private Text bestScoreText;
	
	private MenuScene menuChildScene;
	//private MenuScene optionChildScene;
	
	private final int MENU_PLAY = 0;
	private final int MENU_CONTINUE = 1;
	private final int MENU_OPTIONS = 2;
	private final int MENU_SOUND = 3;
	private final int MENU_BACK = 4;
	
	@Override
	public void createScene() {
		// TODO Auto-generated method stub
		createBackground();
		createMenuChildScene();
		//createOptionChildScene();
	}

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public SceneType getSceneType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * This method handles event when menu item is clicked
	 */
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY){
		switch(pMenuItem.getID()){		
			case MENU_PLAY:				
				SceneManager.getInstance().loadGameScene(engine);
				return true;
			case MENU_CONTINUE:
				//Load game scene with save data
				return true;
			case MENU_OPTIONS:
				//Load option menu
				menuChildScene.detachSelf();
				menuChildScene.dispose();
				createOptionChildScene();
				return true;
			case MENU_SOUND:
				if(ResourcesManager.getInstance().menuMusic.isPlaying()) {
					ResourcesManager.getInstance().menuMusic.pause();
            } else {
            	ResourcesManager.getInstance().menuMusic.play();
            }
				return true;
			case MENU_BACK:
				menuChildScene.detachSelf();
				menuChildScene.dispose();				
				createMenuChildScene();
			default:
				return false;
		}		
	}

	/**
	 * This method creates the MainMenu with all Component
	 */
	private void createBackground() {
		// background
		attachChild(new Sprite(400, 240, resourcesManager.menuBackgroundRegion, vbom)
		{
    		@Override
            protected void preDraw(GLState pGLState, Camera pCamera) 
    		{
                super.preDraw(pGLState, pCamera);
                pGLState.enableDither();
            }
		});
		
		// best score
		attachChild(new Sprite(100,445, resourcesManager.bestScoreRegion, vbom) {
			@Override
            protected void preDraw(GLState pGLState, Camera pCamera) 
    		{
                super.preDraw(pGLState, pCamera);
                pGLState.enableDither();
            }
		});
		
		// minion sprite
		attachChild(new Sprite(150,240, resourcesManager.minionRegion, vbom) {
			@Override
            protected void preDraw(GLState pGLState, Camera pCamera) 
    		{
                super.preDraw(pGLState, pCamera);
                pGLState.enableDither();
            }
		});
		
		// best score text
		bestScoreText = new Text(100, 412, resourcesManager.font, "0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
		bestScoreText.setAnchorCenter(0, 0);	
		bestScoreText.setText("0");
		attachChild(bestScoreText);
	}
	
	/**
	 * This method creates the child scene of the MainMenu (Play, option)
	 */
	private void createMenuChildScene(){
		
		menuChildScene = new MenuScene(camera);		
		
		final IMenuItem playMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_PLAY, resourcesManager.playRegion, vbom), 1, 1);
		//final IMenuItem continueMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_CONTINUE, resourcesManager.playRegion, vbom), 1, 1);
		final IMenuItem optionsMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_OPTIONS, resourcesManager.optionRegion, vbom), 1, 1);
		
		menuChildScene.addMenuItem(playMenuItem);
		//menuChildScene.addMenuItem(continueMenuItem);
		menuChildScene.addMenuItem(optionsMenuItem);
		
		menuChildScene.buildAnimations();
		menuChildScene.setBackgroundEnabled(false);
		
		playMenuItem.setPosition(playMenuItem.getX(), playMenuItem.getY() - 15);
		//continueMenuItem.setPosition(continueMenuItem.getX(), continueMenuItem.getY() - 50);
		optionsMenuItem.setPosition(optionsMenuItem.getX(), optionsMenuItem.getY() - 75);
		
		menuChildScene.setOnMenuItemClickListener(this);
		
		setChildScene(menuChildScene);
	}
	
	/**
	 * This method creates the child scene after pressing Option
	 */
	private void createOptionChildScene(){
		menuChildScene = new MenuScene(camera);
		final IMenuItem soundMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_SOUND, resourcesManager.soundRegion, vbom), 1, 1);
		final IMenuItem backMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_BACK, resourcesManager.backRegion, vbom), 1, 1);
		
		menuChildScene.addMenuItem(soundMenuItem);
		menuChildScene.addMenuItem(backMenuItem);
		
		menuChildScene.buildAnimations();
		menuChildScene.setBackgroundEnabled(false);
				
		soundMenuItem.setPosition(soundMenuItem.getX(), soundMenuItem.getY() - 15);
		backMenuItem.setPosition(backMenuItem.getX(), backMenuItem.getY() - 75);
		
		menuChildScene.setOnMenuItemClickListener(this);
		setChildScene(menuChildScene);
	}
}
